import argparse
import json
import os
import pathlib
import shutil
from subprocess import check_output, run
import sys

import lib


def check_call(*args, **kwargs):
    # subprocess.check_call in stdlib doesn't support `input`
    kwargs['check'] = True
    return run(*args, **kwargs).returncode


def warmup(cache_fn):
    check_call(['git', 'ls-files', '-s'], stdout=args.LOG('oldindex').open('wb'))
    curfiles = [*lib.Git.ls_files(lib.index_filter)]
    if not curfiles:
        return
    with lib.cached_key(cache_fn) as cache:
        newfiles_obj = {fn: obj for _, obj, _, fn in curfiles}
        obj_newfile = {*{obj: fn for fn, obj in newfiles_obj.items()}.values()}  # deduped by obj
        check_call(['git', 'rm', '--cached', '-q', '--', *obj_newfile])
        annexed = check_output(['git', 'annex', 'add', '-q', '--json', '-c', 'annex.alwayscommit=false', *args.ANNEX_ADD, '--', *obj_newfile]).decode().splitlines()
        assert len(annexed) == len(obj_newfile)
        fn_symobj = {fn: obj for mode, obj, _, fn in lib.Git.ls_files() if fn in obj_newfile}
        for result in map(json.loads, annexed):
            assert result['success']
            obj = newfiles_obj[result['file']]
            link = os.readlink(result['file'])
            cache[obj] = link[link.rindex('.git/annex/objects/'):]
            cache[obj, result['file'].count('/')] = fn_symobj[result['file']]

        for mode, obj, stage, fn in curfiles:
            depth = fn.count('/')
            if (obj, depth) not in cache:
                syma = cache[obj]
                symr = os.path.relpath(syma, os.path.dirname(fn))
                cache[obj, depth] = check_output(['git', 'hash-object', '-t', 'blob', '--stdin', '-w'], input=symr.encode('ascii')).strip().decode('ascii')
    check_call(['git', 'ls-files', '-s'], stdout=args.LOG('newindex').open('wb'))
    check_call(['git', 'annex', '-q', 'merge'])  # will be called by `git annex pre-commit`, but called without `-q`
    check_call(['git', 'commit', '-qm', 'git-annex-index-filter-warmup\n\nshould be no-op after `filter`'])


def main(cache_fn):
    check_call(['git', 'ls-files', '-s'], stdout=args.LOG('oldindex').open('wb'))
    curfiles = [*lib.Git.ls_files(lib.index_filter)]
    chgindex = []
    with lib.cached_key(cache_fn) as cache:
        newfiles_obj = {fn: obj for _, obj, _, fn in curfiles if obj not in cache}
        if newfiles_obj:
            obj_newfile = {*{obj: fn for fn, obj in newfiles_obj.items()}.values()}  # deduped by obj

            # from concurrent.futures import ThreadPoolExecutor
            # import threading
            # lock = threading.BoundedSemaphore()
            # def w(arr):
            #     check_call(['git', 'checkout-index', '--prefix', '%s/' % os.getcwd(), '-f', '--', *arr])
            #     check_call(['git', 'update-index', '--force-remove', '--', *arr])
            #     with lock:
            #         annexed = check_output(['git', 'annex', 'add', '--json', '-c', 'annex.alwayscommit=false', *args.ANNEX_ADD, *arr])
            #     return annexed.decode().splitlines()
            # with ThreadPoolExecutor() as e:
            #     for results in e.map(w, lib.chunked(tuple(obj_newfile), 250)):
            #         for result in map(json.loads, results):
            #             assert result['success']
            #             obj = newfiles_obj[result['file']]
            #             cache[obj] = lib.examinekey(result['key'])

            check_call(['git', 'checkout-index', '--prefix', '%s/' % os.getcwd(), '-f', '--', *obj_newfile])
            # check_call(['git', 'rm', '--cached', '-q', '--', *obj_newfile])
            # check_call(['git', 'rm', '--cached', '-qf', '--', *obj_newfile])
            check_call(['git', 'update-index', '--force-remove', '--', *obj_newfile])
            annexed = check_output(['git', 'annex', 'add', '--json', '-c', 'annex.alwayscommit=false', *args.ANNEX_ADD, *obj_newfile]).decode().splitlines()
            assert len(annexed) == len(obj_newfile)

            for result in map(json.loads, annexed):
                assert result['success']
                obj = newfiles_obj[result['file']]
                cache[obj] = lib.examinekey(result['key'])
            # Assure no garbage left
            run(['find', '-type', 'd', '-empty', '-delete'], check=True)
            assert not os.listdir()
        for mode, obj, stage, fn in curfiles:
            depth = fn.count('/')
            if (obj, depth) not in cache:
                syma = cache[obj]
                symr = os.path.relpath(syma, os.path.dirname(fn))
                cache[obj, depth] = check_output(['git', 'hash-object', '-t', 'blob', '--stdin', '-w'], input=symr.encode('ascii')).strip().decode('ascii')
            chgindex.append(lib.t_index('120000', cache[obj, depth], stage, fn))
    chgindex = '\0'.join(chgindex).encode()
    args.LOG('chgindex').write_bytes(chgindex)
    check_call(['git', 'update-index', '-z', '--index-info'], input=chgindex)
    check_call(['git', 'ls-files', '-s'], stdout=args.LOG('newindex').open('wb'))
    check_call(['git', 'annex', '-q', 'merge'])


if __name__ == '__main__':
    os.putenv('GIT_LITERAL_PATHSPECS', '1')
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', nargs='?', default='filter', choices=('warmup', 'filter'))
    parser.add_argument('-d', '--debug', action='store_true', help='Do not remove debugging info.')
    parser.add_argument('ANNEX_ADD', nargs=argparse.REMAINDER, help='Arguments passing to git annex add.')
    args = parser.parse_args()

    args.GIT_COMMIT = os.getenv('GIT_COMMIT', 'warmup')[:7]
    logdir = pathlib.Path('/home/mail6543210/p2p/afftemp/idx2_' + args.GIT_COMMIT)
    logdir.mkdir(parents=True, exist_ok=True)
    args.LOG = logdir.joinpath
    check_call('env|grep -E "^GIT_|^PWD="|sort', shell=True, stdout=args.LOG('environ').open('wb'))

    fn = os.getenv('IF_LOG')
    open(fn, 'ab')
    try:
        shutil.copyfile(fn, str(args.LOG('cache-before')))
        shutil.copystat(fn, str(args.LOG('cache-before')))
        {'warmup': warmup, 'filter': main}[args.mode](fn)
    except Exception as e:
        shutil.copyfile(fn, str(args.LOG('cache-after')))
        shutil.copystat(fn, str(args.LOG('cache-after')))
        print(logdir, file=sys.stderr)
        raise e
    else:
        if args.debug:
            shutil.copyfile(fn, str(args.LOG('cache-after')))
            shutil.copystat(fn, str(args.LOG('cache-after')))
            print(logdir, file=sys.stderr)
        else:
            shutil.rmtree(str(logdir))  # py36
