import argparse
import enum
import hashlib
import os
import pathlib
import shutil
import sys
from subprocess import check_call as c_call, check_output as c_output
from subprocess import DEVNULL, PIPE, Popen
import tempfile

import lib
from lib import examinekey, t_index, cached_key


class Strategy(enum.Enum):
    Noop     = 0
    Move     = 1
    Copy     = 2
    Reflink  = 3
    CopyAuto = 4


GIT_DIR = pathlib.Path(os.getenv('GIT_DIR', '.git'))
STRA = Strategy[os.getenv('STRATEGY')]


TMPDIR = tempfile.TemporaryDirectory(prefix='index-filter-', dir=GIT_DIR / 'annex')
tempfile.tempdir = TMPDIR.name


def calckey_n_write(obj, hash, path):
    if path is None:
        path = pathlib.Path(os.devnull)
    hasher = hashlib.new(hash.lower())
    size = int(c_output(['git', 'cat-file', '-s', obj]))
    size2 = 0
    p = Popen(['git', 'cat-file', 'blob', obj], stdout=PIPE)
    with path.open('wb') as f:
        while p.poll() is None:
            buf = p.stdout.read(128 * 2 ** 20)
            size2 += len(buf)
            f.write(buf)
            hasher.update(buf)
    assert p.returncode == 0, p.returncode
    assert size == size2, (size, size2)
    return size, hasher.hexdigest()


def stage0(cache_fn):
    c_call(['git', 'ls-files', '-s'], stdout=args.LOG('oldindex').open('wb'))
    with cached_key(cache_fn) as cac:
        for mode, obj, stage, fn in lib.Git.ls_files(lib.index_filter):
            if STRA is Strategy.Noop:
                src = None
            else:
                src = pathlib.Path(tempfile.mkstemp()[1])
            size, hexdigest = calckey_n_write(obj, args.backend.rstrip('E'), src)
            key = '%s-s%d--%s%s' % (args.backend, size, hexdigest, os.path.splitext(fn)[1])
            if STRA is not Strategy.Noop:
                syma = examinekey(key)
                cac[obj] = syma
                dst = GIT_DIR.joinpath('..', syma)
                dst.parent.mkdir(parents=True, exist_ok=True)
                if STRA is Strategy.Move:
                    src.rename(dst)
                elif STRA is Strategy.Copy:
                    shutil.copyfile(src, dst)
                    shutil.copystat(src, dst)
                elif STRA is Strategy.Reflink:
                    c_call(['cp', '-a', '--reflink=always', src, dst])
                elif STRA is Strategy.CopyAuto:
                    c_call(['cp', '-a', '--reflink=auto', src, dst])


UUID = c_output(['git', 'config', 'annex.uuid'], universal_newlines=True).strip()


def main(cache_fn):
    c_call(['git', 'ls-files', '-s'], stdout=args.LOG('oldindex').open('wb'))

    chgindex, keys = [], []
    with cached_key(cache_fn) as cac:
        for mode, obj, stage, fn in lib.Git.ls_files(lib.index_filter):
            depth = fn.count('/')
            if (obj, depth) not in cac:
                if obj not in cac:
                    if STRA is Strategy.Noop:
                        src = None
                    else:
                        src = pathlib.Path(tempfile.mkstemp()[1])
                    size, hexdigest = calckey_n_write(obj, args.backend.rstrip('E'), src)
                    key = '%s-s%d--%s%s' % (args.backend, size, hexdigest, os.path.splitext(fn)[1])
                    cac[obj] = examinekey(key)
                    if STRA is not Strategy.Noop:
                        syma = cac[obj]
                        dst = GIT_DIR.joinpath('..', syma)
                        dst.parent.mkdir(parents=True, exist_ok=True)

                        src.rename(dst)
                syma = cac[obj]
                symr = os.path.relpath(syma, os.path.dirname(fn))
                p = c_call(['git', 'hash-object', '-t', 'blob', '--stdin', '-w'], input=symr, stdout=PIPE, universal_newlines=True)
                cac[obj, depth] = p.stdout.strip()
            chgindex.append(t_index('120000', cac[obj, depth], stage, fn))
            keys.append('%s %s 1' % (os.path.basename(cac[obj]), UUID))

    c_call(['git', 'annex', 'setpresentkey', '--batch'], stdout=DEVNULL, input='\n'.join(keys).encode('ascii'))
    chgindex = '\n'.join(chgindex).encode()
    args.LOG('chgindex').write_bytes(chgindex)
    c_call(['git', 'update-index', '--index-info'], input=chgindex)
    c_call(['git', 'ls-files', '-s'], stdout=args.LOG('newindex').open('wb'))
    c_call(['git', 'annex', '-q', 'merge'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('mode', nargs='?', default='filter', choices=('warmup', 'filter'))
    parser.add_argument('-d', '--debug', action='store_true', help='Do not remove debugging info.')
    parser.add_argument('-J', '--jobs', type=int, help='Enable parallel jobs. Accepts 0 to automatically detect cpus. Overrides `ANNEX_ADD` if specified. See git-annex-add(1).')
    parser.add_argument('ANNEX_ADD', nargs=argparse.REMAINDER, help='Arguments passing to git annex add.')

    args = parser.parse_args()
    if args.jobs is not None:
        if args.jobs == 0:
            args.jobs = len(os.sched_getaffinity(0))
        args.ANNEX_ADD.append('-J%d' % args.jobs)
    del args.jobs

    args.GIT_COMMIT = os.getenv('GIT_COMMIT', 'warmup')[:7]
    logdir = pathlib.Path('/home/mail6543210/p2p/afftemp/idx1_' + args.GIT_COMMIT)
    # logdir = tempfile.mkdtemp(prefix=f'/home/mail6543210/p2p/afftemp/idx1_{args.GIT_COMMIT}_')
    logdir.mkdir(parents=True, exist_ok=True)
    args.LOG = logdir.joinpath
    c_call('env|grep -E "^GIT_|^PWD="|sort', shell=True, stdout=args.LOG('environ').open('wb'))

    fn = os.getenv('IF_LOG')
    open(fn, 'ab')
    try:
        shutil.copyfile(fn, str(args.LOG('cache-before')))
        shutil.copystat(fn, str(args.LOG('cache-before')))
        {'warmup': stage0, 'filter': main}[args.mode]()
    except Exception as e:
        shutil.copyfile(fn, str(args.LOG('cache-after')))
        shutil.copystat(fn, str(args.LOG('cache-after')))
        print(logdir, file=sys.stderr)
        raise e
    else:
        if args.debug:
            shutil.copyfile(fn, str(args.LOG('cache-after')))
            shutil.copystat(fn, str(args.LOG('cache-after')))
            print(logdir, file=sys.stderr)
        else:
            shutil.rmtree(logdir)
