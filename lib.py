import contextlib
import functools
import os
import pickle
from subprocess import check_output as c_output
import time

import annex
called = False


@contextlib.contextmanager
def profile(msg):
    global called
    t0 = time.time()
    try:
        yield
    finally:
        if called is False:
            called = True
            print('')
        print('%8.2f\t%s' % (time.time() - t0, msg))


def chunked(arr, size=1):
    while arr:
        chunk, arr = arr[:size], arr[size:]
        yield chunk


def examinekey(key):
    hashdirmixed = annex.hashdirmixed(key)
    return '.git/annex/objects/{hashdirmixed}{key}/{key}'.format(**locals())
    # return f'.git/annex/objects/{hashdirmixed}{key}/{key}'


def t_index(mode, obj, stage, fn):
    return '{mode} {obj} {stage}\t{fn}'.format(**locals())
    # return f'{mode} {obj} {stage}\t{fn}'


@contextlib.contextmanager
def cached_key(fn):
    # Dict[str, str]: (obj): examinekey
    # Dict[Tuple[str, int], str]: ((obj, depth)): symlink-content
    open(fn, 'ab')  # Test if writable
    try:
        # obj2key = json.load(open(fn))
        obj2key = pickle.load(open(fn, 'rb'))
    except EOFError:
        obj2key = {}
    try:
        yield obj2key
    finally:
        # json.dump(obj2key, open(fn, 'w'), skip_keys=True)
        pickle.dump(obj2key, open(fn + '.part', 'wb'), pickle.HIGHEST_PROTOCOL)
        os.rename(fn + '.part', fn)


def _expandargs(func):
    @functools.wraps(func)
    def wrapper(index):
        assert isinstance(index, tuple)
        return func(*index)
    return wrapper


@_expandargs
def index_filter(mode, obj, stage, fn):
    """Return True if the file needs to be annexed."""
    if mode == '120000':
        return False
    # if fn.endswith('.mp4'):
    #     return True
    # if os.path.splitext(fn)[1][1:] in ('dds', 'dll', 'exe', 'icns', 'ico', 'iqm', '.jpg', 'lib', 'mpz', 'ogg', '.png', 'ttf', 'wpt', 'blend', 'tga', 'md3', 'it', 'wav', 'md5mesh', 'md5anim'):
    #     return True
    if os.path.splitext(fn)[1][1:] == 'jpg':
        # return False
        return True
    if os.path.splitext(fn)[1][1:] == 'png':
        # return False
        return True
    return False


class Git:
    @staticmethod
    def _from_index(line):
        meta, fn = line.split('\t')
        return (*meta.split(' '), fn)

    @staticmethod
    def ls_files(filter_func=None):
        """Calls `git ls-files`, parse and filter the results into (mode, obj, stage, fn)."""
        return filter(filter_func,  map(Git._from_index, filter(None, c_output(['git', 'ls-files', '-sz']).strip(b'\0').decode().split('\0'))))
